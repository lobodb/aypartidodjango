from django.db import models

# Create your models here.

class Partido(models.Model):
    contrincantes_text = models.CharField(max_length=200)
    hora = models.TimeField()
    fecha = models.DateField()

