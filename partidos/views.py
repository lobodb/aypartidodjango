from django.shortcuts import render
from django.core import serializers
from django.http import JsonResponse
from .models import Partido

import requests
import json
from bs4 import BeautifulSoup



# Create your views here.
def partidos(request):

    htmldoc = requests.get("http://www.espn.com.co/futbol/equipo/calendario/_/id/2919/league/col.1/once-caldas")
    soup = BeautifulSoup(htmldoc.content, 'lxml')

    data = []

    table = soup.find('table')
    rows = table.find_all('tr')

    for row in rows:
        data_row = []
        for col in row.find_all('td'):
            col_content = col.text.strip()
            if( len( col_content ) > 50 ):
                col_content = col_content[:5]
            data_row.append(col_content)
        if( ( data_row[0] != "Fecha" ) and (len(data_row) > 1 and data_row[1] != "Final") and ( len(data_row) > 5 and data_row[5] == "Estadio Palogrande")  ):
            data.append(data_row)

    json_list = []

    for row in data:
        json_data = {}
        for idx, col in enumerate(row):
            if( idx == 0 ):
                json_data["fecha"] = col
            elif( idx == 1 ):
                json_data["hora"] = col
            elif( idx == 2 ):
                json_data["local"] = "O. Caldas"
            elif( idx == 4 ):
                json_data["visitante"] = col
            elif( idx == 5 ):
                json_data["Estadio"] = "Palogrande"
        json_list.append(json_data)

    for row in json_list:
        print(row)
    return JsonResponse(json_list, safe=False) 
